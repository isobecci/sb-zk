module.exports = {
  root: true,
  env: {
    "es6": true,
    "mocha": true,
    browser: true,
    node: true
  },
  parserOptions: {
    "ecmaVersion": 2017,
    parser: 'babel-eslint'
  },
  "extends": [
    'plugin:vue/recommended',
    // 'plugin:prettier/recommended'
    'prettier',
    'prettier/vue'
  ],
  // required to lint *.vue files
  plugins: ['vue', 'prettier' ],
  // add your custom rules here
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/component-name-in-template-casing': 'off'
  }
  // "rules": {
  //   "indent": [
  //     "error",
  //     2
  //   ],
  //   "linebreak-style": [
  //     "error",
  //     "unix"
  //   ],
  //   "quotes": [
  //     "error",
  //     "single"
  //   ],
  //   "semi": [
  //     "error",
  //     "always"
  //   ]
  // }
}
