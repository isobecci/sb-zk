import Vue from "vue";
import Vuex from "vuex";
import feathersClient from "../plugins/feathers-client";
// import feathersVuex from "feathers-vuex";
// const { service } = feathersVuex(feathersClient, { idField: "_id" });
import feathersVuex, { initAuth } from "feathers-vuex";
const { service, auth, FeathersVuex } = feathersVuex(feathersClient, {
  idField: "_id"
});

// Register the plugin with Vue.
Vue.use(FeathersVuex)

const createStore = () => {
  return new Vuex.Store({
    state: {},
    mutations: {},
    actions: {
      nuxtServerInit({ commit, dispatch }, { req }) {
        return initAuth({
          commit,
          dispatch,
          req,
          moduleName: "auth",
          cookieName: "feathers-jwt"
        });
      }
    },
    plugins: [
      service("users"),
      service("messages"),
      auth({
        state: {
          publicPages: ["login", "signup", "messages"]
        },
        userService: "users"
      })
    ]
  });
};
export default createStore;
