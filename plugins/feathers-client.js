import feathers from "@feathersjs/feathers";
import socketio from "@feathersjs/client/socketio";
import auth from "@feathersjs/client/authentication";
import io from "socket.io-client";
import { CookieStorage } from "cookie-storage";

console.log("logging in the client");
console.log(process.env.baseUrl);

const socket = io(process.env.baseUrl, { transports: ["websocket"] });

const feathersClient = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage: new CookieStorage() }));

export default feathersClient;
