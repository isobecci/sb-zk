const path = require("path");

module.exports = {
  /* Env
   */
  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3030"
  },
  /*
   ** feathers
  */
  rootDir: path.resolve(__dirname),
  dev: process.env.NODE_ENV !== "production",
  /*
   ** Headers of the page
   */
  head: {
    title: 'BONT Cycling Shoes',
    titleTemplate: '%s - BONT JAPAN',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'BONT Cycling Shoes'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },
  /*
   ** add after the head property, add a string that references this routing middleware
   */
  router: {
    middleware: "auth"
  },
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: "#92D3CE"
  },
  /*
   ** Modules
   */
  modules: [],
  plugins: [],
  css: [],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Vendor
     */
    // vendor: [
    //   'babel-polyfill'
    // ],
    optimization: {
      splitChunks: {
        minSize: 30000,
        maxSize: 244000
      }
    },
    extractCSS: true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    // routes: generateDynamicRoutes,
    fallback: true
  },
  manifest: {
    name: 'BONT JAPAN',
    short_name: 'BONT JP',
    description: 'BONT Cycling Shoes',
    lang: 'ja',
    theme_color: '#fff',
    background_color: '#fafafa'
  }
};
